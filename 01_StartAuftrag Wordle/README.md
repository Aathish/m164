# Unit 1 Problemanalyse und Umsetzung

# 1. erstellen eines Datenmodells

Wir analysieren eine einfache Applikation, indem wir für die Datenhaltung wichtige use-cases definieren.

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Kleingruppen |
| Aufgabenstellung  | Ziele/Eigenschaften formulieren, Applikation kennen lernen |
| Zeitbudget  |  2 Lektion |
| Ziel | Grundlagen für die Erstellung des ERMs beschreiben; Dokument mit den nötigen Daten, die zur Speicherung sinnvoll erscheinen |

Das online-Game "Wordle" ist zur Zeit ein Hype im Internet. In der Originalversion kann es nur einmal pro Tag ausgeführt werden, es gibt keine User-Accounts und ist gratis. Das Ziel ist mit maximal 6 Versuchen ein vorgegebenes Wort mit 5 Buchstaben zu finden. Das Original ist von Josh Wardle programmiert, mittlerweile aber an die "New York Times" verkauft worden und somit natürlich auf englisch. Es ist seit Kurzem ebenfalls in einer deutschen Version verfügbar.

Auf den ersten Blick sieht es nicht nach einem grossen Datenmodell aus. Erst wenn Sie sich genauer mit der Applikation beschäftigen und mögliche Abfragen überlegen, sehen Sie, dass es als Datenbank durchaus sinnvoll ist.

# 2. Links
- Das Original der New York Times: https://www.nytimes.com/games/wordle/index.html
- Version auf deutsch ohne Umlaute: https://wordle.at/
- Version auf deutsch mit Umlauten: https://wordle-deutsch.ch/
- allowed guesses: https://gist.github.com/cfreshman/cdcdf777450c5b5301e439061d29694c
- answers: https://gist.github.com/cfreshman/a03ef2cba789d8cf00c08f767e0fad7b
- Infos allgemein (leider bei beiden Anbietern nur für registrierte Abonnenten). Tagesanzeiger: https://www.tagesanzeiger.ch/so-meistern-sie-die-fuenf-buchstaben-507383416054 oder NZZ: https://www.nzz.ch/panorama/wordle-ein-raetsel-begeistert-das-netz-ld.1664267?reduced=true

# 3. Datenbank implementieren

Aus dem erstellten logischen, relationalen Datenmodell soll eine relationale Datenbank in einem RDBMS (relationales Datenbankmanagementsystem) erstellt werden. Inklusive Beziehungen, Constraints, Datentypen ...

# 4. Daten abfüllen

Wörterlisten stehen auf Gitsystemen im Internet zur Verfügung. Diese müssen in die Datenbank importiert werden. Das funktioniert nicht ausschliesslich mit einfachen Importbefehlen oder SQL-INSERTS.
Andere Tabelleninhalte können eventuell mit Testdatengeneratoren erzeugt werden oder Sie können auch eigene Skripte dazu schreiben.